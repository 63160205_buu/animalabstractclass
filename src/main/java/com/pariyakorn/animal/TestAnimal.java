/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.animal;

/**
 *
 * @author acer
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));

        Animal a1 = h1;
        System.out.println("a1 is land animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal ? " + (a1 instanceof Reptile));

        Cat c1 = new Cat("Moo");
        c1.eat();
        c1.walk();
        c1.run();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is land animal ? " + (c1 instanceof LandAnimal));

        Animal a2 = c1;
        System.out.println("a2 is poultry animal ? " + (a2 instanceof Poultry));
        System.out.println("a2 is land animal ? " + (a2 instanceof LandAnimal));

        Dog d1 = new Dog("Boo");
        d1.eat();
        d1.walk();
        d1.run();
        System.out.println("d1 is animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is land animal ? " + (d1 instanceof LandAnimal));

        Animal a3 = d1;
        System.out.println("a3 is aquatic animal ? " + (a3 instanceof AquaticAnimal));
        System.out.println("a3 is land animal ? " + (a3 instanceof LandAnimal));

        Crocodile c2 = new Crocodile("numnim");
        c2.eat();
        c2.walk();
        c2.crawl();
        System.out.println("c2 is animal ? " + (c2 instanceof Animal));
        System.out.println("c2 is reptile animal ? " + (c2 instanceof Reptile));

        Animal a4 = c2;
        System.out.println("a4 is reptile animal ? " + (a4 instanceof Reptile));
        System.out.println("a4 is land animal ? " + (a4 instanceof LandAnimal));

        Snake s1 = new Snake("kai");
        s1.eat();
        s1.crawl();
        System.out.println("s1 is animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is reptile animal ? " + (s1 instanceof Reptile));

        Animal a5 = s1;
        System.out.println("a5 is poultry animal ? " + (a5 instanceof Poultry));
        System.out.println("a5 is reptile animal ? " + (a5 instanceof Reptile));

        Fish f1 = new Fish("Deedoo");
        f1.eat();
        f1.swim();
        f1.sleep();
        System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is aquatic animal ? " + (f1 instanceof AquaticAnimal));

        Animal a6 = f1;
        System.out.println("a6 is aquaticanimal ? " + (a6 instanceof AquaticAnimal));
        System.out.println("a6 is reptile animal ? " + (a6 instanceof Reptile));

        Crab c3 = new Crab("kami");
        c3.eat();
        c3.swim();
        c3.sleep();
        System.out.println("c3 is animal ? " + (c3 instanceof Animal));
        System.out.println("c3 is aquatic animal ? " + (c3 instanceof AquaticAnimal));

        Animal a7 = c3;
        System.out.println("a7 is aquaticanimal ? " + (a7 instanceof AquaticAnimal));
        System.out.println("a7 is poultry  animal ? " + (a7 instanceof Poultry));

        Bat b1 = new Bat("hunki");
        b1.eat();
        b1.fly();
        b1.sleep();
        System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is poultry animal ? " + (b1 instanceof Poultry));

        Animal a8 = b1;
        System.out.println("a8 is aquaticanimal ? " + (a8 instanceof AquaticAnimal));
        System.out.println("a8 is poultry  animal ? " + (a8 instanceof Poultry));
        
        Bird b2 = new Bird("bok");
        b2.eat();
        b2.fly();
        b2.sleep();
        System.out.println("b2 is animal ? " + (b2 instanceof Animal));
        System.out.println("b2 is poultry animal ? " + (b2 instanceof Poultry));

        Animal a9 = b2;
        System.out.println("a9 is reptile animal ? " + (a9 instanceof Reptile));
        System.out.println("a9 is poultry  animal ? " + (a9 instanceof Poultry));
    }

}
